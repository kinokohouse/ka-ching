//
//  TextField.m
//  Ka-Ching!
//
//  Created by Petros on 05-01-18.
//  Copyright (c) 2017-2018 Kinoko House. MIT License.
//

#import "TextField.h"
#import "AppDelegate.h"

@implementation TextField

- (void)textDidChange:(NSNotification *)notification {
    NSText *editor = [self currentEditor];
    NSRange range = [editor selectedRange];
    NSUInteger loc = range.location;
    NSMutableString *currentText = [[self stringValue] mutableCopy];
    if ([currentText length] > 10) {
        NSBeep();
		[currentText deleteCharactersInRange:NSMakeRange(loc - 1, 1)];
        [self setStringValue:currentText];
        [[self currentEditor] setSelectedRange:NSMakeRange(loc - 1, 0)];
    }
}

@end


