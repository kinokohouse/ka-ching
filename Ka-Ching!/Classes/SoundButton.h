//
//  SoundButton.h
//  File Type & Creator Editor
//
//  Created by Petros on 05-01-18.
//  Copyright (c) 2017-2018 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@interface SoundButton : NSButton


#pragma mark Class Methods : Clicks

- (void)rightMouseUp:(NSEvent *)event;
- (void)mouseDown:(NSEvent *)event;


#pragma mark Class Methods : Drag and Drop

- (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender;
- (void)draggingExited:(id<NSDraggingInfo>)sender;
- (NSDragOperation)performDragOperation:(id<NSDraggingInfo>)sender;
- (NSDragOperation)concludeDragOperation:(id<NSDraggingInfo>)sender;

@end
