//
//  AppDelegate.h
//  Ka-Ching!
//
//  Created by Petros on 05-01-18.
//  Copyright (c) 2017-2018 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>
#import <Carbon/Carbon.h>
#import <Foundation/Foundation.h>
#import "HotKeys.h"


@interface AppDelegate : NSObject <NSApplicationDelegate, NSSoundDelegate, NSPopoverDelegate, NSUserNotificationCenterDelegate>


#pragma mark Exposed IBOutlets

@property (weak) IBOutlet NSPanel *window;


#pragma mark Exposed Application Properties

@property BOOL showNotifications;
@property BOOL popoverClosingAllowedFlag;
@property BOOL preferencesWindowOpen;
@property BOOL popoverOpen;
@property BOOL popoverOpenedFromDrag;
@property NSUInteger bankSelection;

#pragma mark Exposed Application Methods

- (IBAction)showWindow:(id)sender;
- (void)playSound:(id)sender;
- (void)removeSound:(id)sender;
- (void)respondToHotkey:(int)hotkey;
- (NSArray *)getHotkeyNecessities;
- (void)handleRightClickOnButan:(id)sender;
- (void)setTimerForSoundButtonDragItem:(NSArray *)objects;

@end

