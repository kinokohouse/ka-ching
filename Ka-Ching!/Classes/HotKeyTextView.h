//
//  HotKeyTextView.h
//  Ka-Ching!
//
//  Created by Petros Loukareas on 24-01-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//


# pragma mark Includes

#import <Cocoa/Cocoa.h>
#import <AppKit/AppKit.h>
#import <Foundation/Foundation.h>

# pragma mark Forward Class Declarations

@class HotkeyCenter;


@interface HotkeyTextView : NSTextView

// objects initialised from nib
@property (retain) IBOutlet HotkeyCenter *hotkeyCenter;

#pragma mark Exposed Class Properties
@property int keyCode;
@property unsigned long modMask;
@property NSString *keyString;
@property NSDictionary *keyDict;

#pragma mark IBActions
- (IBAction)clearField:(id)sender;
- (IBAction)snapBack:(id)sender;

#pragma mark Class Constructors
- (instancetype)initWithCoder:(NSCoder *)decoder;

#pragma mark Class Delegate Methods
- (void)keyDown:(NSEvent *)event;

#pragma mark Exposed Class Methods
- (void)setDataWithKey:(int)keycode andModMask:(unsigned long)modmask withRepresentation:(NSString *)keyString andSnapBack:(BOOL)flag;
- (void)setDataFromArray:(NSArray *)array withSnapback:(BOOL)flag;

@end
