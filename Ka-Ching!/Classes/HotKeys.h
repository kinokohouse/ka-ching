//
//  HotKeys.h
//  Ka-Ching!
//
//  Created by Petros on 05-01-18.
//  Copyright (c) 2017-2018 Kinoko House. MIT License.
//


# pragma mark Includes

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import <Carbon/Carbon.h>
#import "HotKeyTextView.h"


@interface HotkeyCenter : NSObject

#pragma mark Properties
@property NSMutableArray *hotkeys;
@property NSDictionary *keyEquivDict;

#pragma mark Class Constructors & Destructors
- (id)init;
- (void)dealloc;

#pragma mark Class Methods: Hotkey Registration
- (void)registerHotkey:(unsigned long)keyCode withModifiers:(unsigned long)modMask andRepresentation:(NSString *)keyString forTaskID:(int)taskid;
- (void)unregisterHotkeyForTaskId:(int)taskid;
- (void)pauseHotkeyForTaskId:(int)taskid;
- (void)unregisterHotkeys;
- (void)pauseHotkeys;
- (void)unpauseHotkeys;

#pragma mark Class Methods: Utilities
- (NSArray *)getHotkeysAsArray;
- (NSArray *)getHotkeyForTaskIdAsArray:(int)taskid;
- (void)registerHotkeysFromArray:(NSArray *)array;
- (NSString *)hotKeyToMenuKeyEquivalent:(int)keycode;
- (NSArray *)prepHotkeysForPropertyListStorage;
- (BOOL)validateInput:(id)obj forKeyCombo:(NSString *)keycombo checkRegisteredHotkeysOnly:(BOOL)flag;

@end


