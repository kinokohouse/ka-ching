//
//  HotKeyTextView.m
//  Ka-Ching!
//
//  Created by Petros Loukareas on 24-01-18.
//  Copyright © 2018 Kinoko House. All rights reserved.
//

#pragma mark Includes

#import "HotKeyTextView.h"
#import "AppDelegate.h"


#pragma mark Constants

// if below const is NO, then at least either ctrl or cmd is mandatory
const BOOL ALLOW_NO_CMD_KEYS = NO;

// if const below is YES, check of contents of HotkeyTextFields is skipped and only completely registered hotkeys are checked
const BOOL CHECK_REGISTERED_HOTKEYS_ONLY = YES;

// below const speaks for itself
const BOOL APPDELEGATE_HANDLES_HOTKEY_VALIDATION = NO;


#pragma mark -

@interface HotkeyTextView()

#pragma mark Unexposed Class Properties

@property BOOL isSet;
@property int prevKeyCode;
@property unsigned long prevModMask;
@property NSString *prevKeyString;

@end


@implementation HotkeyTextView


#pragma mark IBActions

- (IBAction)clearField:(id)sender {
    if ((self.keyString == nil) || ([self.keyString isEqualToString:@""])) {
        return;
    } else {
        self.prevKeyCode = self.keyCode;
        self.prevModMask = self.modMask;
        self.prevKeyString = self.keyString;
        self.font = [NSFont fontWithName:@".LucidaGrandeUI" size:13.0];
        self.textColor = [NSColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
        [self setString:@"[not set]"];
        self.keyCode = 0;
        self.modMask = 0;
        self.keyString = @"";
        _isSet = NO;
        [self setSelectionColor];
    }
}

- (IBAction)snapBack:(id)sender {
    if ((self.prevKeyString == nil) || ([self.prevKeyString isEqualToString:@""])) {
        return;
    } else {
        NSString *prevKeyString = self.prevKeyString;
        BOOL result = [self validateInput:[self identifier] forKeyCombo:prevKeyString checkRegisteredHotkeysOnly:CHECK_REGISTERED_HOTKEYS_ONLY];
        if (result) {
            self.keyCode = self.prevKeyCode;
            self.modMask = self.prevModMask;
            self.keyString = self.prevKeyString;
            self.prevKeyCode = 0;
            self.prevModMask = 0;
            self.prevKeyString = @"";
            self.font = [NSFont fontWithName:@".LucidaGrandeUI" size:13.0];
            self.textColor = [NSColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
            [self setString:self.keyString];
        } else {
            [self clearField:nil];
        }
    }
}


#pragma mark Class Constructors

- (instancetype)initWithCoder:(NSCoder *)decoder {
    self = [super initWithCoder:decoder];
    if (self) {
        [self getKeyDict];
    }
    return self;
}

#pragma mark Class Delegate Methods

- (void)keyDown:(NSEvent *)event {
    unsigned long modflags = (unsigned long)event.modifierFlags;
    int keycode = event.keyCode;
    if ((modflags == self.modMask) && (keycode == self.keyCode)) {
        return;
    }
    if (!ALLOW_NO_CMD_KEYS) {
        if (((modflags & NSControlKeyMask) == 0) && (((modflags & NSCommandKeyMask) == 0))) {
            NSBeep();
            return;
        }
    }
    NSString *keycombo = @"";
    if ((modflags & NSControlKeyMask) > 0) {
        keycombo = [NSString stringWithFormat:@"%@⌃", keycombo];
    }
    if ((modflags & NSAlternateKeyMask) > 0) {
        keycombo = [NSString stringWithFormat:@"%@⌥", keycombo];
    }
    if ((modflags & NSShiftKeyMask) > 0) {
        keycombo = [NSString stringWithFormat:@"%@⇧", keycombo];
    }
    if ((modflags & NSCommandKeyMask) > 0) {
        keycombo = [NSString stringWithFormat:@"%@⌘", keycombo];
    }
    NSString *downKey = [self findSymbolForKey:keycode];
    if (downKey == nil) {
        NSBeep();
        return;
    } else {
        keycombo = [NSString stringWithFormat:@"%@%@", keycombo, [self findSymbolForKey:keycode]];
        BOOL result = [self validateInput:[self identifier] forKeyCombo:keycombo checkRegisteredHotkeysOnly:CHECK_REGISTERED_HOTKEYS_ONLY];
        if (result == NO) {
            NSBeep();
            return;
        }
        self.font = [NSFont fontWithName:@".LucidaGrandeUI" size:13.0];
        self.textColor = [NSColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
        [self setString:keycombo];
        self.prevKeyCode = self.keyCode;
        self.prevModMask = self.modMask;
        self.prevKeyString = self.keyString;
        self.keyCode = keycode;
        self.modMask = modflags;
        self.keyString = keycombo;
        _isSet = YES;
        [self setSelectionColor];
    }
}


#pragma mark Class Methods

- (void)getKeyDict {
    if (!_keyDict) {
        _keyDict = [NSDictionary dictionaryWithObjectsAndKeys:
                    @0x00, @"A",
                    @0x0B, @"B",
                    @0x08, @"C",
                    @0x02, @"D",
                    @0x0E, @"E",
                    @0x03, @"F",
                    @0x05, @"G",
                    @0x04, @"H",
                    @0x22, @"I",
                    @0x26, @"J",
                    @0x28, @"K",
                    @0x25, @"L",
                    @0x2E, @"M",
                    @0x2D, @"N",
                    @0x1F, @"O",
                    @0x23, @"P",
                    @0x0C, @"Q",
                    @0x0F, @"R",
                    @0x01, @"S",
                    @0x11, @"T",
                    @0x20, @"U",
                    @0x09, @"V",
                    @0x0D, @"W",
                    @0x07, @"X",
                    @0x10, @"Y",
                    @0x06, @"Z",
                    @0x1D, @"0",
                    @0x12, @"1",
                    @0x13, @"2",
                    @0x14, @"3",
                    @0x15, @"4",
                    @0x17, @"5",
                    @0x16, @"6",
                    @0x1A, @"7",
                    @0x1C, @"8",
                    @0x19, @"9",
                    @0x32, @"`",
                    @0x0A, @"§",
                    @0x2B, @",",
                    @0x2F, @".",
                    @0x1B, @"-",
                    @0x18, @"=",
                    @0x2C, @"/",
                    @0x2A, @"\\",
                    @0x27, @"'",
                    @0x29, @";",
                    @0x1E, @"]",
                    @0x21, @"[",
                    @0x7A, @"F1",
                    @0x78, @"F2",
                    @0x63, @"F3",
                    @0x76, @"F4",
                    @0x60, @"F5",
                    @0x61, @"F6",
                    @0x62, @"F7",
                    @0x64, @"F8",
                    @0x65, @"F9",
                    @0x6D, @"F10",
                    @0x67, @"F11",
                    @0x6F, @"F12",
                    @0x69, @"F13",
                    @0x6B, @"F14",
                    @0x71, @"F15",
                    @0x35, @"⎋",
                    @0x31, @"SPACE",
                    @0x24, @"↩",
                    @0x4C, @"⌤",
                    @0x30, @"⇥",
                    @0x33, @"⌫",
                    @0x7B, @"←",
                    @0x7C, @"→",
                    @0x7D, @"↓",
                    @0x7E, @"↑",
                    nil];
    }
}

- (NSString *)findSymbolForKey:(int)keyCode {
    NSArray *keyArray = [_keyDict allKeysForObject:[NSNumber numberWithInt:keyCode]];
    if ([keyArray count] == 0) {
        return nil;
    } else {
        return [keyArray objectAtIndex:0];
    }
}

- (void)setDataWithKey:(int)keycode andModMask:(unsigned long)modmask withRepresentation:(NSString *)keyString andSnapBack:(BOOL)flag {
    if (flag) {
        if ([self.keyString isNotEqualTo:@""]) {
            self.prevKeyCode = self.keyCode;
            self.prevModMask = self.modMask;
            self.prevKeyString = self.keyString;
        }
    }
    if ([keyString isEqualToString:@""]) {
        self.font = [NSFont fontWithName:@".LucidaGrandeUI" size:13.0];
        self.textColor = [NSColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
        [self setString:@"[not set]"];
        self.keyString = @"";
        self.keyCode = 0;
        self.modMask = 0;
    } else {
        self.font = [NSFont fontWithName:@".LucidaGrandeUI" size:13.0];
        self.textColor = [NSColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
        [self setString:keyString];
        self.keyString = keyString;
        self.keyCode = keycode;
        self.modMask = modmask;
    }
}

- (void)setDataFromArray:(NSArray *)array withSnapback:(BOOL)flag {
    int keycode = [array[0] intValue];
    unsigned long modmask = [array[1] unsignedLongValue];
    NSString *keystring = [array[2] copy];
    [self setDataWithKey:keycode andModMask:modmask withRepresentation:keystring andSnapBack:flag];
}

- (BOOL)validateInput:(id)obj forKeyCombo:(NSString *)keycombo checkRegisteredHotkeysOnly:(BOOL)flag {
    BOOL result = YES;
    if (APPDELEGATE_HANDLES_HOTKEY_VALIDATION) {
        // message to app delegate to handle it, but for now let's just return a YES
        return result;
    }
    result = [_hotkeyCenter validateInput:(id)obj forKeyCombo:(NSString *)keycombo checkRegisteredHotkeysOnly:(BOOL)flag];
    return result;
}

- (void)setSelectionColor {
    NSColor *foregroundSelectionColor;
    if (_isSet) {
        foregroundSelectionColor = [NSColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
    } else {
        foregroundSelectionColor = [NSColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0];
    }
    
    [self setSelectedTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSColor whiteColor], NSBackgroundColorAttributeName,
                                      foregroundSelectionColor, NSForegroundColorAttributeName,
                                      nil]];
}

@end
