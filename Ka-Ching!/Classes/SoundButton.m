//
//  SoundButton.m
//  File Type & Creator Editor
//
//  Created by Petros on 05-01-18.
//  Copyright (c) 2017-2018 Kinoko House. MIT License.
//

#import "SoundButton.h"
#import "AppDelegate.h"


@interface SoundButton()


#pragma mark Unexposed Class Properties

@property NSMutableArray *objectsToPass;

@end


@implementation SoundButton


#pragma mark Class Methods : Clicks

- (void)rightMouseUp:(NSEvent *)event {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    [appDelegate handleRightClickOnButan:self];
}

- (void)mouseDown:(NSEvent *)theEvent {
    [super mouseDown:theEvent];
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    if ([theEvent modifierFlags] & NSControlKeyMask) {
        [appDelegate handleRightClickOnButan:self];
    } else if ([theEvent modifierFlags] & NSAlternateKeyMask) {
        [appDelegate removeSound:self];
    } else {
        [appDelegate playSound:self];
    }
}


#pragma mark Class Methods : Drag and Drop

- (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    if (appDelegate.preferencesWindowOpen || appDelegate.popoverOpen) {
        return NO;
    }
    NSArray <NSString *> *fileTypes = @[@"wav", @"wave", @"mp3", @"aif", @"aiff", @"m4a"];
    if (appDelegate.bankSelection == 0) {
        return NO;
    }
    NSPasteboard *pb = [sender draggingPasteboard];
    NSArray *theFiles = [pb propertyListForType:NSFilenamesPboardType];
    if (theFiles == nil) {
        return NO;
    }
    NSString *firstFile = [theFiles objectAtIndex:0];
    NSWorkspace *workSpace = [NSWorkspace sharedWorkspace];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL result = [workSpace isFilePackageAtPath:firstFile];
    if (result) {
        return NO;
    }
    BOOL isDir;
    result = [fileManager fileExistsAtPath:firstFile isDirectory:&isDir];
    if (isDir) {
        return NO;
    }
    NSString *ext = [[firstFile pathExtension] lowercaseString];
    if ([fileTypes containsObject:ext]) {
        [appDelegate showWindow:nil];
        [self setFocusRingType:NSFocusRingTypeExterior];
        [appDelegate.window makeFirstResponder:self];
        return YES;
    }
    return NO;
}

- (void)draggingExited:(id<NSDraggingInfo>)sender {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    [self setFocusRingType:NSFocusRingTypeNone];
    [appDelegate.window makeFirstResponder:nil];
}

- (NSDragOperation)performDragOperation:(id<NSDraggingInfo>)sender {
    if (_objectsToPass) {
        [_objectsToPass removeAllObjects];
    } else {
        _objectsToPass = [NSMutableArray arrayWithCapacity:2];
    }
    NSPasteboard *pb = [sender draggingPasteboard];
    NSArray *theFiles = [pb propertyListForType:NSFilenamesPboardType];
    [_objectsToPass addObject:self];
    [_objectsToPass addObject:[theFiles objectAtIndex:0]];
    return YES;
}

- (NSDragOperation)concludeDragOperation:(id<NSDraggingInfo>)sender {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    [self setFocusRingType:NSFocusRingTypeNone];
    [appDelegate setTimerForSoundButtonDragItem:_objectsToPass];
    return YES;
}

@end

