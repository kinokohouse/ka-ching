//
//  main.m
//  Ka-Ching!
//
//  Created by Petros on 05-01-18.
//  Copyright (c) 2017-2018 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
