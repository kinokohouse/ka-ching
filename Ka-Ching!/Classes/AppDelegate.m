//
//  AppDelegate.m
//  Ka-Ching!
//
//  Created by Petros on 05-01-18.
//  Copyright (c) 2017-2018 Kinoko House. MIT License.
//

#import "AppDelegate.h"


@interface AppDelegate ()


#pragma mark IBOutlets

// Main Window IBOutlets
@property (weak) IBOutlet NSMenu *statusMenu;
@property (weak) IBOutlet NSButton *stahpButan;
@property (weak) IBOutlet NSButton *sound1Butan;
@property (weak) IBOutlet NSButton *sound2Butan;
@property (weak) IBOutlet NSButton *sound3Butan;
@property (weak) IBOutlet NSButton *sound4Butan;
@property (weak) IBOutlet NSSlider *windowSlider;
@property (weak) IBOutlet NSPopUpButton *bankSelectionPopUpButan;

// Menu IBOutlets
@property (weak) IBOutlet NSSlider *menuSlider;
@property (weak) IBOutlet NSMenuItem *stahpMenuItem;
@property (weak) IBOutlet NSMenuItem *sound1MenuItem;
@property (weak) IBOutlet NSMenuItem *sound2MenuItem;
@property (weak) IBOutlet NSMenuItem *sound3MenuItem;
@property (weak) IBOutlet NSMenuItem *sound4MenuItem;
@property (weak) IBOutlet NSMenuItem *bankSelection1MenuItem;
@property (weak) IBOutlet NSMenuItem *bankSelection2MenuItem;
@property (weak) IBOutlet NSMenuItem *bankSelection3MenuItem;
@property (weak) IBOutlet NSMenuItem *bankSelection1PopUpMenuItem;
@property (weak) IBOutlet NSMenuItem *bankSelection2PopUpMenuItem;
@property (weak) IBOutlet NSMenuItem *bankSelection3PopUpMenuItem;
@property (weak) IBOutlet NSMenuItem *prefsMenuItem;

// Popover IBOutlets
@property (weak) IBOutlet NSBox *divLine;
@property (weak) IBOutlet NSButton *snapBackButan;
@property (weak) IBOutlet NSButton *removeHotkeyButan;
@property (weak) IBOutlet NSPopover *popOver;
@property (weak) IBOutlet NSTextField *nameEntryTextField;
@property (retain) IBOutlet NSTextField *popoverTitle;
@property (retain) IBOutlet HotkeyTextView *hotKeyTextView;

// Preference panel IBOutlets
@property (weak) IBOutlet NSWindow *prefWindow;
@property (weak) IBOutlet NSButton *animateMenuSymbolCheckBox;
@property (weak) IBOutlet NSButton *cacophoniaCheckBox;
@property (weak) IBOutlet NSButton *irritatingClickCheckBox;
@property (weak) IBOutlet NSButton *showNotificationsCheckBox;
@property (weak) IBOutlet NSButton *rememberWindowStateCheckBox;
@property (weak) IBOutlet NSButton *alwaysOnTopCheckBox;
@property (weak) IBOutlet NSButton *openAtLoginCheckBox;
@property (weak) IBOutlet NSPopUpButton *bankSwitchOptionPopUpButan;
@property (retain) IBOutlet HotkeyTextView *stahpHotKey;

// objects initialised from nib
@property (retain) IBOutlet HotkeyCenter *hotkeyCenter;

#pragma mark Application Properties

@property BOOL rememberWindowState;
@property BOOL animateMenuSymbol;
@property BOOL bankSwitchOption;
@property BOOL nowPlaying;
@property BOOL openAtLogin;
@property BOOL cacophonia;
@property BOOL irritatingClick;
@property BOOL soundboardAlwaysOnTop;
@property float relativeVolume;
@property float oldVolume;
@property NSArray *senderStorage;
@property NSArray *hotkeyBackup;
@property NSArray *bellAnimationArray;
@property NSTimer *allPurposeTimer;
@property NSTimer *animationTimer;
@property NSTimer *notificationDismissalTimer;
@property NSUInteger buttonID;
@property NSUInteger animFrameCount;
@property NSMutableArray *soundNameArray;
@property NSMutableArray *menuItemCollection;
@property NSMutableArray *butanCollection;
@property NSMutableArray <NSSound *> *soundCollection;
@property NSMutableArray *soundArray;
@property (retain) NSStatusBar *statusBar;
@property (retain) NSStatusItem *statusItem;
@property NSUserNotificationCenter *notificationCenter;
@property NSMutableArray <HotkeyTextView *> *hotKeyTextViewCollection;

@end


#pragma mark -

@implementation AppDelegate


#pragma mark App Delegate Methods

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    _menuItemCollection = [NSMutableArray arrayWithCapacity:8];
    [_menuItemCollection addObject:_stahpMenuItem];
    [_menuItemCollection addObject:_sound1MenuItem];
    [_menuItemCollection addObject:_sound2MenuItem];
    [_menuItemCollection addObject:_sound3MenuItem];
    [_menuItemCollection addObject:_sound4MenuItem];
    [_menuItemCollection addObject:_bankSelection1MenuItem];
    [_menuItemCollection addObject:_bankSelection2MenuItem];
    [_menuItemCollection addObject:_bankSelection3MenuItem];
     _butanCollection  = [NSMutableArray arrayWithCapacity:8];
    [_butanCollection addObject:_stahpButan];
    [_butanCollection addObject:_sound1Butan];
    [_butanCollection addObject:_sound2Butan];
    [_butanCollection addObject:_sound3Butan];
    [_butanCollection addObject:_sound4Butan];
    [_butanCollection addObject:_bankSelection1PopUpMenuItem];
    [_butanCollection addObject:_bankSelection2PopUpMenuItem];
    [_butanCollection addObject:_bankSelection3PopUpMenuItem];
    _hotKeyTextViewCollection  = [NSMutableArray arrayWithCapacity:2];
    [_hotKeyTextViewCollection addObject:_hotKeyTextView];
    [_hotKeyTextViewCollection addObject:_stahpHotKey];
    _soundCollection = [NSMutableArray arrayWithCapacity:4];
    for (int i = 0; i < 4; i++) {
        [_soundCollection addObject:[NSSound soundNamed:@"Tink"]];
    }
     NSAppearance *appearance = [NSAppearance appearanceNamed:NSAppearanceNameAqua];
    [_hotKeyTextView setAppearance:appearance];
    [_nameEntryTextField setAppearance:appearance];
    [_snapBackButan setAppearance:appearance];
    [_removeHotkeyButan setAppearance:appearance];
    [_divLine setAppearance:appearance];
    _preferencesWindowOpen = NO;
    _popoverOpen = NO;
    _notificationCenter = [NSUserNotificationCenter defaultUserNotificationCenter];
    [_notificationCenter setDelegate:self];
    _statusBar = [NSStatusBar systemStatusBar];
    _statusItem = [_statusBar statusItemWithLength:NSSquareStatusItemLength];
    [_statusItem setMenu:_statusMenu];
    NSImage *bellCenterImage = [NSImage imageNamed:@"bell-center"];
    NSImage *bellLeftImage = [NSImage imageNamed:@"bell-left"];
    NSImage *bellRightImage = [NSImage imageNamed:@"bell-right"];
    [bellCenterImage setTemplate:YES];
    [bellLeftImage setTemplate:YES];
    [bellRightImage setTemplate:YES];
    _bellAnimationArray = @[bellLeftImage, bellCenterImage, bellRightImage, bellCenterImage];
    [_statusItem.button setImage:bellCenterImage];
    _soundArray = [NSMutableArray arrayWithCapacity:3];
    [self voidStopButton];
    for (int i = 1; i < 5; i++) {
        [_butanCollection[i] registerForDraggedTypes:@[@"public.file-url"]];
    }
    [self voidStopButton];
    [self getPreferences];
    _oldVolume = _relativeVolume;
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender {
    if (_preferencesWindowOpen || _popoverOpen) {
        return NSTerminateCancel;
    }
    return NSTerminateNow;
}

- (void)applicationWillTerminate:(NSNotification *)notification {
    [self setPreferences];
}


#pragma mark User Notification Center Delegate Methods

- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center shouldPresentNotification:(NSUserNotification *)notification {
    [self setPreferences];
    return YES;
}



#pragma mark IBActions : General

- (IBAction)stopSound:(id)sender {
    _nowPlaying = 0;
    for (int i = 0; i < 4; i++) {
        [_soundCollection[i] stop];
    }
    if (_animateMenuSymbol) {
        [self stopAnimation];
    }
    [self voidStopButton];
}

- (IBAction)showWindow:(id)sender {
    [NSApp activateIgnoringOtherApps:YES];
    [_window makeKeyAndOrderFront:self];
}

- (IBAction)bankSelectMenuAction:(id)sender {
    NSUInteger oldBankSelection = _bankSelection;
    NSUInteger calledFrom = [[sender identifier] integerValue];
    _bankSelection = calledFrom - 15;
    if (_bankSelection == oldBankSelection) {
        return;
    }
    [_bankSelectionPopUpButan selectItemAtIndex:_bankSelection];
    [self bankSelectionPopUpButanHasChanged:sender];
    if (_showNotifications) {
        [self notificationForChangedSoundBank];
    }
}

#pragma mark IBActions : Preferences

- (IBAction)showPrefs:(id)sender {
    if (_preferencesWindowOpen) {
        [NSApp activateIgnoringOtherApps:YES];
        return;
    }
    if (!_popoverOpen) {
        if (_soundboardAlwaysOnTop) {
            [_window setLevel:NSNormalWindowLevel];
        }
        _preferencesWindowOpen = YES;
        [self resetPreferences];
        [self disableBankSelection];
        [_hotkeyCenter pauseHotkeys];
        [_prefWindow makeKeyAndOrderFront:self];
        [NSApp activateIgnoringOtherApps:YES];
    }
}

- (IBAction)acceptPrefs:(id)sender {
    _showNotifications = [_showNotificationsCheckBox state];
    _animateMenuSymbol = [_animateMenuSymbolCheckBox state];
    _rememberWindowState = [_rememberWindowStateCheckBox state];
    _openAtLogin = [_openAtLoginCheckBox state];
    _soundboardAlwaysOnTop = [_alwaysOnTopCheckBox state];
    _cacophonia = [_cacophoniaCheckBox state];
    _irritatingClick = [_irritatingClickCheckBox state];
    [self addToLoginItemsOrNot];
    NSUInteger selection = [_bankSwitchOptionPopUpButan indexOfSelectedItem];
    if (selection != _bankSwitchOption) {
        NSArray *keyCodes = @[@0x12, @0x13, @0x14];
        NSArray *modMasks = @[@786721UL, @1573160UL, @393475UL];
        NSArray *keyStubs = @[@"⌃⌥", @"⌥⌘", @"⌃⇧"];
        unsigned long modMask = [modMasks[selection] unsignedLongValue];
        NSString *keyStub = keyStubs[selection];
        for (int i = 5; i < 8; i++) {
            [_hotkeyCenter registerHotkey:[keyCodes[i-5] intValue] withModifiers:modMask andRepresentation:[NSString stringWithFormat:@"%@%@", keyStub, [NSString stringWithFormat:@"%i", (int)i-4]] forTaskID:i];
        }
    }
    _bankSwitchOption = selection;
    if ([_stahpHotKey.keyString isNotEqualTo:@""]) {
        [_hotkeyCenter registerHotkey:_stahpHotKey.keyCode withModifiers:_stahpHotKey.modMask andRepresentation:_stahpHotKey.keyString forTaskID:0];
    } else {
        [_hotkeyCenter unregisterHotkeyForTaskId:0];
    }
    [_prefWindow orderOut:nil];
    _preferencesWindowOpen = NO;
    if (_soundboardAlwaysOnTop) {
        [_window setLevel:NSModalPanelWindowLevel];
    }
    [self enableBankSelection];
    [_hotkeyCenter unpauseHotkeys];
    [self setMenuKeyEquivalentsFromHotkeys:[_hotkeyCenter getHotkeysAsArray] onlySounds:NO];
    [self updateOnlyHotKeysPrefs];
}

- (IBAction)cancelPrefs:(id)sender {
    [_prefWindow orderOut:nil];
    _preferencesWindowOpen = NO;
    if (_soundboardAlwaysOnTop) {
        [_window setLevel:NSModalPanelWindowLevel];
    }
    [self enableBankSelection];
    [self resetPreferences];
    [_hotkeyCenter unpauseHotkeys];
}

- (IBAction)bankSelectionPopUpButanHasChanged:(id)sender {
    NSUInteger oldBankSelection = _bankSelection;
    int calledFrom = [[sender identifier] intValue] % 10;
    if (!(calledFrom >= 5 && calledFrom <=7)) {
        calledFrom = (int)[_bankSelectionPopUpButan indexOfSelectedItem] + 5;
    }
    for (NSUInteger i = 5; i < 8; i++) {
        if ([[_menuItemCollection[i] identifier] integerValue] % 10 == calledFrom) {
            [_menuItemCollection[i] setState:NSOnState];
        } else {
            [_menuItemCollection[i] setState:NSOffState];
        }
    }
    _bankSelection = [_bankSelectionPopUpButan indexOfSelectedItem];
    [self setMenuTitlesFromSoundArray];
    if (_bankSelection == oldBankSelection) {
        return;
    }
    if (_showNotifications) {
        [self notificationForChangedSoundBank];
    }
}

- (IBAction)restoreHotkeys:(id)sender {
    [self resetHotkeys];
    [self resetStahpHotKey];
}

- (void)resetStahpHotKey {
    NSArray *stopHotkey = [_hotkeyCenter getHotkeysAsArray][0];
    int keyc = 0;
    unsigned long modm = 0;
    NSString *keys = @"";
    if (![stopHotkey isEqual:[NSNull null]]) {
        keyc = [stopHotkey[0] intValue];
        modm = [stopHotkey[1] unsignedLongValue];
        keys = stopHotkey[2];
    }
    _stahpHotKey.keyCode = keyc;
    _stahpHotKey.modMask = modm;
    _stahpHotKey.keyString = keys;
    [_stahpHotKey setDataWithKey:keyc andModMask:modm withRepresentation:keys andSnapBack:NO];
}

- (IBAction)openSoundsFolder:(id)sender {
    NSString *locPath = [[[NSFileManager defaultManager] URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil] path];
    locPath = [NSString stringWithFormat:@"%@/Ka-Ching!/", locPath];
    NSURL *locURL = [NSURL fileURLWithPath:locPath];
    [[NSWorkspace sharedWorkspace] openURL: locURL];
}


#pragma mark Application Methods : Audio Player & Related Controls

- (void)playSound:(id)sender {
    NSString *path;
    if (!_cacophonia) {
        [self stopSound:nil];
    }
    NSUInteger senderId = [[sender identifier] integerValue];
    senderId = senderId % 10;
    if (_cacophonia) {
        [self stopOnlyThisSound:senderId - 1];
    }
    NSArray *soundSubArray = _soundArray[_bankSelection];
    NSArray *keyArray = soundSubArray[senderId - 1];
    NSString *fileNameBit = keyArray[0];
    NSString *fileExt = keyArray[2];
    NSString *fileName = [NSString stringWithFormat:@"%@.%@", fileNameBit, fileExt];
    if ([fileNameBit isEqualToString:@""]) {
        NSBeep();
        if (_cacophonia) {
            if (![self isAnythingPlaying]) {
                [self voidStopButton];
            }
        }
        return;
        }
    if (_bankSelection == 0) {
        path = [[NSBundle mainBundle] resourcePath];
    } else {
        path = [[[NSFileManager defaultManager] URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil] path];
        path = [NSString stringWithFormat:@"%@/%@", path, @"Ka-Ching!"];
    }
    path = [NSString stringWithFormat:@"%@/%@", path, fileName];
    [self voidStopButton];
    NSURL *url = [NSURL fileURLWithPath:path];
    unsigned long voice = senderId - 1;
    NSError *error = nil;
    _soundCollection[voice] = [[NSSound alloc] initWithContentsOfURL:url byReference:NO];
    if (error) {
        NSLog(@"%@", error);
        return;
    }
    [_soundCollection[voice] setVolume:_relativeVolume];
    [_soundCollection[voice] play];
    [self resetStopButton];
    if (_animateMenuSymbol) {
        [self startAnimation];
    }
}

- (void)stopOnlyThisSound:(NSUInteger)sound {
    if (_soundCollection[sound].playing) {
        [_soundCollection[sound] stop];
    }
}

- (void)removeSound:(id)sender {
    if (_bankSelection == 0) {
        return;
    }
    NSUInteger soundMorselPointer = [[sender identifier] integerValue] - 1;
    NSMutableArray *soundSubArray = [_soundArray[_bankSelection] mutableCopy];
    NSMutableArray *soundMorsel = [soundSubArray[soundMorselPointer] mutableCopy];
    NSString *soundFileName = soundMorsel[0];
    if ([soundFileName isEqualToString:@""]) {
        [sender setTitle:NSLocalizedString(@"Beep", nil)];
        return;
    }
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *soundFileExt = soundMorsel[2];
    NSString *path = [NSString stringWithFormat:@"%@/Ka-Ching!/%@.%@", [[fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:&error] path], soundFileName, soundFileExt];
    if (error) {
        NSLog(@"Cannot construct path for sound file removal. Details below:");
        NSLog(@"%@", error);
        [self notificationWithTitle:NSLocalizedString(@"Something went wrong...", nil) body:NSLocalizedString(@"Couldn't remove sound on request. Try restarting your computer. Sorry for the inconvenience.", nil) withSound:NO];
        return;
    }
    error = nil;
    BOOL fileRemoved = [fileManager removeItemAtPath:path error:&error];
    if (!fileRemoved) {
        NSLog(@"Cannot delete sound file. Sound may have been deleted already. Details below:");
        NSLog(@"%@", error);
        [self notificationWithTitle:NSLocalizedString(@"Something went wrong...", nil) body:[NSString stringWithFormat:NSLocalizedString(@"Sound file \"%@.%@\" may have already been removed. Restoring button anyway.", nil), soundFileName, soundFileExt] withSound:NO];
    }
    [soundMorsel replaceObjectAtIndex:0 withObject:@""];
    [soundMorsel replaceObjectAtIndex:1 withObject:NSLocalizedString(@"Beep", nil)];
    [soundMorsel replaceObjectAtIndex:2 withObject:@"sys"];
    [soundSubArray replaceObjectAtIndex:soundMorselPointer withObject:soundMorsel];
    [_soundArray replaceObjectAtIndex:_bankSelection withObject:soundSubArray];
    [sender setTitle:NSLocalizedString(@"Beep", nil)];
    [self stopSound:nil];
    NSSound *clickPlay = [NSSound soundNamed:@"Tink"]; // walks amok
    clickPlay.volume = 1.0;
    [clickPlay play];
    if (fileRemoved) {
        [self notificationWithTitle:NSLocalizedString(@"Sound succesfully removed", nil) body:[NSString stringWithFormat:NSLocalizedString(@"The button has been restored to the system beep for this soundboard.", nil)] withSound:NO];
    }
}

- (void)voidStopButton {
    [_stahpButan setEnabled:NO];
    [_stahpMenuItem setEnabled:NO];
}

- (void)resetStopButton {
    [_stahpButan setEnabled:YES];
    [_stahpMenuItem setEnabled:YES];
}

- (void)disableBankSelection {
    [_bankSelectionPopUpButan setEnabled:NO];
    for (int i = 0; i < 3; i++) {
        [_menuItemCollection[i+5] setEnabled:NO];
    }
}

- (void)enableBankSelection {
    [_bankSelectionPopUpButan setEnabled:YES];
    for (int i = 0; i < 3; i++) {
        [_menuItemCollection[i+5] setEnabled:YES];
    }
}

- (IBAction)respondToVolumeSlider:(id)sender {
    if (_relativeVolume != _oldVolume) {
        for (int i = 0; i < 4; i++) {
            if (_soundCollection[i] != nil) {
                _soundCollection[i].volume = _relativeVolume;
            }
        }
        if (_irritatingClick) {
            NSSound *beep = [NSSound soundNamed:@"Tink"];
            beep.volume = _relativeVolume;
            [beep play];
        }
        _oldVolume = _relativeVolume;
    }
}

#pragma mark Application Methods : Hotkeys

- (void)respondToHotkey:(int)hotkey {
    if (hotkey == 0) {
        [self stopSound:nil];
        return;
    }
    if ((1 <= hotkey) && (hotkey <= 4)) {
        [self playSound:_butanCollection[hotkey]];
        return;
    }
    if ((5 <= hotkey) && (hotkey <= 7)) {
        [_bankSelectionPopUpButan selectItemAtIndex:hotkey - 5];
        [self bankSelectionPopUpButanHasChanged:_bankSelectionPopUpButan];
        return;
    }   
}

- (void)resetHotkeys {
    _bankSwitchOption = 0;
    [_bankSwitchOptionPopUpButan selectItemAtIndex:_bankSwitchOption];
    NSArray *newhotkeys = @[@[@0x0C, @786721ul, @"⌃⌥Q", @"", @""],
                            @[@0x1F, @786721ul, @"⌃⌥O", @"", @""],
                            @[@0x23, @786721ul, @"⌃⌥P", @"", @""],
                            @[@0x25, @786721ul, @"⌃⌥L", @"", @""],
                            @[@0x29, @786721ul, @"⌃⌥;", @"", @""],
                            @[@0x12, @786721ul, @"⌃⌥1", @"", @""],
                            @[@0x13, @786721ul, @"⌃⌥2", @"", @""],
                            @[@0x14, @786721ul, @"⌃⌥3", @"", @""]];
    [_hotkeyCenter registerHotkeysFromArray:newhotkeys];
    [self setMenuKeyEquivalentsFromHotkeys:newhotkeys onlySounds:NO];
}

- (void)setMenuKeyEquivalentsFromHotkeys:(NSArray *)array onlySounds:(BOOL)flag {
    int keycode;
    unsigned long modmask;
    NSString *keyequiv;
    NSArray *hotKeyMorsel;
    unsigned long emptymask = (unsigned long)@256;
    int i;
    int j = 8;
    if (flag) {
        j = 5;
    }
    for (i = 0; i < j; i++) {
        hotKeyMorsel = array[i];
        if ([hotKeyMorsel isNotEqualTo:[NSNull null]]) {
            keycode = [hotKeyMorsel[0] intValue];
            keyequiv = [_hotkeyCenter hotKeyToMenuKeyEquivalent:keycode];
            modmask = [[hotKeyMorsel objectAtIndex:1] unsignedLongValue];
            [_menuItemCollection[i] setKeyEquivalent:keyequiv];
            [_menuItemCollection[i] setKeyEquivalentModifierMask:modmask];
            if (i > 4) {
                [_butanCollection[i] setKeyEquivalent:keyequiv];
                [_butanCollection[i] setKeyEquivalentModifierMask:modmask];
            }
        } else {
            [_menuItemCollection[i] setKeyEquivalent:@""];
            [_menuItemCollection[i] setKeyEquivalentModifierMask:emptymask];
            if (i > 4) {
                [_butanCollection[i] setKeyEquivalent:@""];
                [_butanCollection[i] setKeyEquivalentModifierMask:emptymask];
            }
        }
    }
}

- (NSArray *)getHotkeyNecessities {
    return @[[_hotKeyTextViewCollection copy], [NSNumber numberWithUnsignedInteger:_buttonID]];
}

#pragma mark Application Methods : Preferences

- (void)getPreferences {
    NSPoint windowPos = [self getDefaultWindowPosition];
    NSString *windowPoint = NSStringFromPoint(windowPos);
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *basicDefaults = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @[@[@[@"kaching",  NSLocalizedString(@"Ka-Ching!", nil), @"mp3"],
                                        @[@"modern",  NSLocalizedString(@"Receipt", nil),   @"mp3"],
                                        @[@"nosale",  NSLocalizedString(@"No Sale", nil),   @"mp3"],
                                        @[@"ping",    NSLocalizedString(@"Ping", nil),      @"mp3"]],
                                      @[@[@"",        NSLocalizedString(@"Beep", nil),      @"sys"],
                                        @[@"",        NSLocalizedString(@"Beep", nil),      @"sys"],
                                        @[@"",        NSLocalizedString(@"Beep", nil),      @"sys"],
                                        @[@"",        NSLocalizedString(@"Beep", nil),      @"sys"]],
                                      @[@[@"",        NSLocalizedString(@"Beep", nil),      @"sys"],
                                        @[@"",        NSLocalizedString(@"Beep", nil),      @"sys"],
                                        @[@"",        NSLocalizedString(@"Beep", nil),      @"sys"],
                                        @[@"",        NSLocalizedString(@"Beep", nil),      @"sys"]]], @"soundArray",
                                    @[@[@0x0C, @786721ul, @"⌃⌥Q", @"", @""],
                                      @[@0x1F, @786721ul, @"⌃⌥O", @"", @""],
                                      @[@0x23, @786721ul, @"⌃⌥P", @"", @""],
                                      @[@0x25, @786721ul, @"⌃⌥L", @"", @""],
                                      @[@0x29, @786721ul, @"⌃⌥;", @"", @""],
                                      @[@0x12, @786721ul, @"⌃⌥1", @"", @""],
                                      @[@0x13, @786721ul, @"⌃⌥2", @"", @""],
                                      @[@0x14, @786721ul, @"⌃⌥3", @"", @""]], @"hotKeys",
                                    @0, @"bankSelection",
                                    @0, @"bankSwitchOption",
                                    @YES, @"showNotifications",
                                    @YES, @"rememberWindowState",
                                    @NO, @"windowVisible",
                                    windowPoint, @"windowPosition",
                                    @YES, @"animateMenuSymbol",
                                    @YES, @"openAtLogin",
                                    @NO, @"soundboardAlwaysOnTop",
                                    @1.0f, @"relativeVolume",
                                    @NO, @"cacophony",
                                    @NO, @"irritatingClick",
                                  nil];
    [userDefaults registerDefaults:basicDefaults];
    _soundArray = [[userDefaults arrayForKey:@"soundArray"] mutableCopy];
    _bankSelection = [userDefaults integerForKey:@"bankSelection"];
    [_bankSelectionPopUpButan selectItemAtIndex:_bankSelection];
    [self bankSelectMenuAction:_menuItemCollection[_bankSelection + 5]];
    _bankSwitchOption = [userDefaults integerForKey:@"bankSwitchOption"];
    [_bankSwitchOptionPopUpButan selectItemAtIndex:_bankSwitchOption];
    _showNotifications = [userDefaults boolForKey:@"showNotifications"];
    [_showNotificationsCheckBox setState:_showNotifications];
    _rememberWindowState = [userDefaults boolForKey:@"rememberWindowState"];
    [_rememberWindowStateCheckBox setState:_rememberWindowState];
    _animateMenuSymbol = [userDefaults boolForKey:@"animateMenuSymbol"];
    [_animateMenuSymbolCheckBox setState:_animateMenuSymbol];
    _openAtLogin = [userDefaults boolForKey:@"openAtLogin"];
    [_openAtLoginCheckBox setState:_openAtLogin];
    [self addToLoginItemsOrNot];
    _soundboardAlwaysOnTop = [userDefaults boolForKey:@"soundboardAlwaysOnTop"];
    [_alwaysOnTopCheckBox setState:_soundboardAlwaysOnTop];
    NSString *windowPosition = [userDefaults stringForKey:@"windowPosition"];
    NSPoint windowOrigin = NSPointFromString(windowPosition);
    BOOL windowVisible = [userDefaults boolForKey:@"windowVisible"];
    if (windowVisible) {
        [self showWindow:nil];
    }
    [_bankSelectionPopUpButan selectItemAtIndex:_bankSelection];
    NSMutableArray *hotKeys = [[userDefaults arrayForKey:@"hotKeys"] mutableCopy];
    [self setMenuKeyEquivalentsFromHotkeys:hotKeys onlySounds:NO];
    [self setMenuTitlesFromSoundArray];
    NSArray *hotKeyMorsel;
    for (int i = 0; i < [hotKeys count]; i++) {
        hotKeyMorsel = hotKeys[i];
        if ([hotKeyMorsel[2] isEqualToString:@""]) {
            [hotKeys replaceObjectAtIndex:i withObject:[NSNull null]];
        }
    }
    [_hotkeyCenter registerHotkeysFromArray:hotKeys];
    [self setMenuKeyEquivalentsFromHotkeys:hotKeys onlySounds:NO];
    if (_rememberWindowState) {
        [_window setFrameOrigin:windowOrigin];
        if (windowVisible) {
            [_window makeKeyAndOrderFront:self];
        }
    } else {
        [_window setFrameOrigin:windowPos];
    }
    if (_soundboardAlwaysOnTop) {
        [_window setLevel:NSModalPanelWindowLevel];
    } else {
        [_window setLevel:NSNormalWindowLevel];
    }
    _relativeVolume = [userDefaults floatForKey:@"relativeVolume"];
    _windowSlider.floatValue = _relativeVolume;
    _menuSlider.floatValue = _relativeVolume;
    _cacophonia = [userDefaults boolForKey:@"cacophony"];
    [_cacophoniaCheckBox setState:_cacophonia];
    _irritatingClick = [userDefaults boolForKey:@"irritatingClick"];
    [_irritatingClickCheckBox setState:_irritatingClick];
    [self prepareAppSupportAmenities];
}

- (void)setPreferences {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:_soundArray forKey:@"soundArray"];
    [userDefaults setInteger:_bankSelection forKey:@"bankSelection"];
    [userDefaults setInteger:[_bankSwitchOptionPopUpButan indexOfSelectedItem] forKey:@"bankSwitchOption"];
    [userDefaults setBool:_showNotifications forKey:@"showNotifications"];
    [userDefaults setBool:_rememberWindowState forKey:@"rememberWindowState"];
    [userDefaults setBool:_animateMenuSymbol forKey:@"animateMenuSymbol"];
    [userDefaults setBool:_cacophonia forKey:@"cacophony"];
    [userDefaults setBool:_irritatingClick forKey:@"irritatingClick"];
    [userDefaults setFloat:_relativeVolume forKey:@"relativeVolume"];
    [userDefaults setBool:[_openAtLoginCheckBox state] forKey:@"openAtLogin"];
    NSPoint windowPos = [_window frame].origin;
    NSString *windowPosition = NSStringFromPoint(windowPos);
    [userDefaults setObject:windowPosition forKey:@"windowPosition"];
    [userDefaults setBool:[_window isVisible] forKey:@"windowVisible"];
    NSArray *hotKeys = [_hotkeyCenter prepHotkeysForPropertyListStorage];
    [userDefaults setObject:hotKeys forKey:@"hotKeys"];
}

- (void)updateOnlySoundArrayPrefs {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:_soundArray forKey:@"soundArray"];
}

- (void)updateOnlyHotKeysPrefs {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *hotKeys = [_hotkeyCenter prepHotkeysForPropertyListStorage];
    [userDefaults setObject:hotKeys forKey:@"hotKeys"];
}

- (void)resetPreferences {
    [_bankSwitchOptionPopUpButan selectItemAtIndex:_bankSwitchOption];
    [_showNotificationsCheckBox setState:_showNotifications];
    [self resetStahpHotKey];
    [_animateMenuSymbolCheckBox setState:_animateMenuSymbol];
    [_cacophoniaCheckBox setState:_cacophonia];
    [_irritatingClickCheckBox setState:_irritatingClick];
    [_rememberWindowStateCheckBox setState:_rememberWindowState];
    [_openAtLoginCheckBox setState:_openAtLogin];
}

- (void)prepareAppSupportAmenities {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    BOOL isDir;
    NSURL *appSupportURL = [fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:&error];
    if (error) {
        NSLog(@"Error while accessing application support directory; details below:");
        NSLog(@"%@", error);
        return;
    }
    NSString *pathToAppSupportDir = [appSupportURL path];
    NSString *pathToMyAppSupportDir = [pathToAppSupportDir stringByAppendingPathComponent:@"Ka-Ching!"];
    NSURL *URLToMyAppSupportDir = [NSURL fileURLWithPath:pathToMyAppSupportDir];
    BOOL folderExists = [fileManager fileExistsAtPath:pathToMyAppSupportDir isDirectory:&isDir];
    if (folderExists && !isDir) {
        NSLog(@"Cannot continue - file with same name as application support folder exists.");
        return;
    }
    if (!folderExists) {
        BOOL directoryCreated = [fileManager createDirectoryAtURL:URLToMyAppSupportDir withIntermediateDirectories:YES attributes:nil error:&error];
        if (!directoryCreated) {
            NSLog(@"Error while creating application support directory; details below:");
            NSLog(@"%@", error);
            return;
        }
    }
}


#pragma mark Application Methods : Popover

- (void)setTimerForSoundButtonDragItem:(NSArray *)objects {
    _popoverOpenedFromDrag = YES;
    _senderStorage = objects;
    _allPurposeTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(handleSoundFileDrop) userInfo:nil repeats:NO];
}

- (void)handleSoundFileDrop {
    if (_preferencesWindowOpen || _popoverOpen) {
        return;
    }
    [_hotkeyCenter pauseHotkeys];
    [self disableBankSelection];
    [_popoverTitle setStringValue:NSLocalizedString(@"Ace, you're about to add a new sound!", nil)];
    _popoverClosingAllowedFlag = NO;
    NSControl *senderObject = _senderStorage[0];
    NSString *fileName = _senderStorage[1];
    fileName = [fileName lastPathComponent];
    fileName = [fileName stringByDeletingPathExtension];
    if (fileName.length > 10) {
        fileName = [fileName substringWithRange:NSMakeRange(0, 10)];
    }
    [_nameEntryTextField setStringValue:fileName];
    NSArray *hotKeyMorsel;
    if ([[_hotkeyCenter getHotkeysAsArray][[[senderObject identifier] intValue]] isEqual:[NSNull null]]) {
        hotKeyMorsel = @[[NSNumber numberWithInt:0],[NSNumber numberWithUnsignedLong:0], @"", @"", @""];
    } else {
        hotKeyMorsel = [_hotkeyCenter getHotkeysAsArray][[[senderObject identifier] intValue]];
    }
    [_hotKeyTextView setDataWithKey:[hotKeyMorsel[0] intValue] andModMask:[hotKeyMorsel[1] unsignedLongValue] withRepresentation:hotKeyMorsel[2] andSnapBack:NO];
    [self showPopover:senderObject];
}

- (void)handleRightClickOnButan:(id)sender {
    if (_preferencesWindowOpen || _popoverOpen) {
        return;
    }
    [_hotkeyCenter pauseHotkeys];
    [self disableBankSelection];
    [_popoverTitle setStringValue:NSLocalizedString(@"So, you'd like to edit the settings then?", nil)];
    _popoverClosingAllowedFlag = NO;
    NSArray *hotKeyMorsel;
    NSArray *soundSubArray = _soundArray[_bankSelection];
    NSArray *keyArray = soundSubArray[[[sender identifier] intValue] - 1];
    NSString *screenName = keyArray[1];
    [_nameEntryTextField setStringValue:screenName];
    if ([[_hotkeyCenter getHotkeysAsArray][[[sender identifier] intValue]] isEqual:[NSNull null]]) {
        hotKeyMorsel = @[[NSNumber numberWithInt:0],[NSNumber numberWithUnsignedLong:0], @"", @"", @""];
    } else {
        hotKeyMorsel = [_hotkeyCenter getHotkeysAsArray][[[sender identifier] intValue]];
    }
    [_hotKeyTextView setDataWithKey:[hotKeyMorsel[0] intValue] andModMask:[hotKeyMorsel[1] unsignedLongValue] withRepresentation:hotKeyMorsel[2] andSnapBack:NO];
    [self showPopover:sender];
}

- (IBAction)acceptPopoverChanges:(id)sender {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSMutableArray *soundSubArray = [_soundArray[_bankSelection] mutableCopy];
    NSMutableArray *soundMorsel = [soundSubArray[_buttonID - 1] mutableCopy];
    NSError *error = nil;
    if (_popoverOpenedFromDrag) {
        NSString *appSupportPath = [[fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil] path];
        if (![soundMorsel[0] isEqualToString:@""]) {
            NSString *oldName = [NSString stringWithFormat:@"%@.%@", soundMorsel[0], soundMorsel[2]];
            NSString *oldPath = [[fileManager URLForDirectory:NSApplicationSupportDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil] path];
            oldPath = [NSString stringWithFormat:@"%@/Ka-Ching!/%@", appSupportPath, oldName];
            BOOL fileRemoved = [fileManager removeItemAtPath:oldPath error:&error];
            if (!fileRemoved) {
                NSLog(@"Error while removing old sound file from application support folder, details below.");
                NSLog(@"This error is non-fatal (just a missing file probs), so we're letting it slide for now.");
                NSLog(@"%@", error);
            }
        }
        NSString *newName = [NSString stringWithFormat:@"%i%i", (int)_bankSelection, (int)_buttonID];
        NSString *sourcePath = _senderStorage[1];
        NSString *fileExt = [sourcePath pathExtension];
        fileExt = [fileExt lowercaseString];
        NSString *newPath = [NSString stringWithFormat:@"%@/Ka-Ching!/%@.%@", appSupportPath, newName, fileExt];
        BOOL result = [fileManager copyItemAtPath:sourcePath toPath:newPath error:&error];
        if (!result) {
            NSLog(@"Error while copying sound file to application support folder, details below:");
            NSLog(@"%@", error);
            NSLog(@"This is probably because the sounds folder is out of sync and and old file still exists");
            NSLog(@"We'll try to remove the offending file; the button has been reset to default \"Beep\"");
            NSLog(@"You should be able to try again after that...");
            BOOL fileRemovedAgain = [fileManager removeItemAtPath:newPath error:&error];
            if (!fileRemovedAgain) {
                NSLog(@"Alas, attempting to remove the old file didn't help.");
                NSLog(@"Here's the error for that portion of this fiasco:");
                NSLog(@"%@", error);
                [self notificationWithTitle:NSLocalizedString(@"Something went wrong...", nil) body:[NSString stringWithFormat:NSLocalizedString(@"Please open the Sounds folder from within Preferences and try removing \"%@.%@\" by hand.", nil), newName, fileExt] withSound:NO];
            }
            newName = @"";
            fileExt = @"mp3";
            [_nameEntryTextField setStringValue:NSLocalizedString(@"Beep", nil)];
            [self notificationWithTitle:NSLocalizedString(@"Something went wrong... BUT!", nil) body:NSLocalizedString(@"...You are encouraged to try again; we tried some fixes. Go on then!", nil) withSound:NO];
        }
        soundMorsel[0] = newName;
        soundMorsel[2] = fileExt;
    }
    soundMorsel[1] = [_nameEntryTextField stringValue];
    [soundSubArray replaceObjectAtIndex:_buttonID - 1 withObject:soundMorsel];
    [_soundArray replaceObjectAtIndex:_bankSelection withObject:soundSubArray];
    if ([_hotKeyTextView.keyString isNotEqualTo:@""]) {
        [_hotkeyCenter registerHotkey:_hotKeyTextView.keyCode withModifiers:_hotKeyTextView.modMask andRepresentation:_hotKeyTextView.keyString forTaskID:(int)_buttonID];
    } else {
        [_hotkeyCenter unregisterHotkeyForTaskId:(int)_buttonID];
    }
    [self setMenuKeyEquivalentsFromHotkeys:[_hotkeyCenter hotkeys] onlySounds:YES];
    [self setMenuTitlesFromSoundArray];
    [self updateOnlySoundArrayPrefs];
    _popoverClosingAllowedFlag = YES;
    [self enableBankSelection];
    [self closePopover];
}

- (IBAction)rejectPopoverChanges:(id)sender {
    [self updateOnlyHotKeysPrefs];
    _popoverClosingAllowedFlag = YES;
    [self enableBankSelection];
    [self closePopover];
}

- (void)showPopover:(id)sender {
    _popoverOpen = YES;
    [_prefsMenuItem setEnabled:NO];
    _buttonID = [[sender identifier] integerValue];
    [_popOver showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxYEdge];
}

- (void)closePopover {
    [_popOver performClose:nil];
    [_hotkeyCenter unpauseHotkeys];
}

- (BOOL)popoverShouldClose:(NSPopover *)popover {
    if (!_popoverClosingAllowedFlag) {
        return _popoverClosingAllowedFlag;
    }
    _popoverOpen = NO;
    _popoverOpenedFromDrag = NO;
    [_prefsMenuItem setEnabled:YES];
    return _popoverClosingAllowedFlag;
}


#pragma mark Application Methods : Status Item Animation

- (void)startAnimation {
    if (!_nowPlaying) {
        _animFrameCount = 0;
        [_statusItem.button setImage:_bellAnimationArray[0]];
        _animationTimer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(animateStatusItem) userInfo:nil repeats:YES];
        _nowPlaying = YES;
    }
}

- (void)stopAnimation {
    if (_animationTimer) {
        [_animationTimer invalidate];
    }
    [_statusItem.button setImage:_bellAnimationArray[1]];
    _nowPlaying = NO;
    [self voidStopButton];
}

- (void)animateStatusItem {
    BOOL anyPlaying = [self isAnythingPlaying];
    if (anyPlaying) {
        _animFrameCount++;
        if (_animFrameCount > 3) {
            _animFrameCount = 0;
        }
        [_statusItem.button setImage:_bellAnimationArray[_animFrameCount]];
    } else {
        [self stopAnimation];
    }
}

- (BOOL)isAnythingPlaying {
    BOOL anyPlaying = NO;
    for (int i = 0; i < 4; i++) {
        if (_soundCollection[i].playing) {
            anyPlaying = YES;
        }
    }
    return anyPlaying;
}


#pragma mark Application Methods : User Notifications

- (void)notificationForChangedSoundBank {
    NSArray *bankStringArray = @[NSLocalizedString(@"Standard Sound Board", nil), NSLocalizedString(@"Custom Sound Board A", nil), NSLocalizedString(@"Custom Sound Board B", nil)];
    NSString *bankString = bankStringArray[_bankSelection];
    NSArray *subSoundArray = _soundArray[_bankSelection];
    NSUserNotification *notification = [[NSUserNotification alloc] init];
    notification.title = [NSString stringWithFormat:NSLocalizedString(@"Switched to %@", nil), bankString];
    notification.informativeText = [NSString stringWithFormat:NSLocalizedString(@"Sounds in this set are \"%@\", \"%@\", \"%@\" and \"%@\".", nil), subSoundArray[0][1], subSoundArray[1][1], subSoundArray[2][1], subSoundArray[3][1]];
    notification.soundName = nil;
    [_notificationCenter deliverNotification:notification];
    if (_notificationDismissalTimer) {
        [_notificationDismissalTimer invalidate];
        _notificationDismissalTimer = nil;
    }
    _notificationDismissalTimer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(dismissNotification) userInfo:nil repeats:NO];
}

- (void)dismissNotification {
    [_notificationCenter removeAllDeliveredNotifications];
}

- (void)notificationWithTitle:(NSString *)title body:(NSString *)body withSound:(BOOL)flag {
    NSUserNotification *notification = [[NSUserNotification alloc] init];
    notification.title = title;
    notification.informativeText = body;
    notification.soundName = nil;
    if (flag) {
        notification.soundName = NSUserNotificationDefaultSoundName;
    }
    [_notificationCenter deliverNotification:notification];
}

#pragma mark Application Methods : Add to Login Items

- (void)addToLoginItemsOrNot {
    LSSharedFileListRef loginItems = LSSharedFileListCreate(NULL, kLSSharedFileListSessionLoginItems, NULL);
    if (loginItems) {
        if (_openAtLogin) {
            [self addSelfToLoginItems];
        } else {
            [self removeSelfFromLoginItems];
        }
    }
    if (loginItems) CFRelease(loginItems);
}

- (void)addSelfToLoginItems {
    CFURLRef theURL = (CFURLRef)CFBridgingRetain([NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]);
    LSSharedFileListRef theLoginItemsRefs = LSSharedFileListCreate(NULL, kLSSharedFileListSessionLoginItems, NULL);
    LSSharedFileListItemRef item = LSSharedFileListInsertItemURL(theLoginItemsRefs, kLSSharedFileListItemLast, NULL, NULL, theURL, NULL, NULL);
    if (item) CFRelease(item);
    if (theLoginItemsRefs) CFRelease(theLoginItemsRefs);
    if (theURL) CFRelease(theURL);
}

- (void)removeSelfFromLoginItems {
    CFURLRef theURL = (CFURLRef)CFBridgingRetain([NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]);
    LSSharedFileListRef theLoginItemsRefs = LSSharedFileListCreate(NULL, kLSSharedFileListSessionLoginItems, NULL);
    UInt32 seedValue;
    UInt32 resolutionFlags = kLSSharedFileListNoUserInteraction | kLSSharedFileListDoNotMountVolumes;
    NSArray  *loginItemsArray = (NSArray *)CFBridgingRelease(LSSharedFileListCopySnapshot(theLoginItemsRefs, &seedValue));
    for (id item in loginItemsArray) {
        LSSharedFileListItemRef itemRef = (LSSharedFileListItemRef)CFBridgingRetain(item);
        CFURLRef currentItemURL = LSSharedFileListItemCopyResolvedURL(itemRef, resolutionFlags, NULL);
        if (currentItemURL && CFEqual(currentItemURL, theURL)) {
            LSSharedFileListItemRemove(theLoginItemsRefs, itemRef);
            CFRelease(currentItemURL);
        }
    }
    if (theURL) CFRelease(theURL);
    if (theLoginItemsRefs) CFRelease(theLoginItemsRefs);
}


#pragma mark Application Methods : Utilities

- (void)setMenuTitlesFromSoundArray {
    NSArray *selectedBank = [_soundArray objectAtIndex:_bankSelection];
    NSString *name;
    for (int i = 1; i < 5; i++) {
        name = [[selectedBank objectAtIndex:i - 1] objectAtIndex:1];
        [[_menuItemCollection objectAtIndex:i] setTitle:name];
        [[_butanCollection objectAtIndex:i] setTitle:name];
    }
}

- (NSPoint)getDefaultWindowPosition {
    NSRect theFrame = [[_statusItem valueForKey:@"window"] frame];
    int x = theFrame.origin.x;
    int y = theFrame.origin.y;
    int w = theFrame.size.width;
    CGFloat newx = x + (w / 2) - 149;
    CGFloat newy = y - 300;
    return NSMakePoint(newx, newy);
}

@end
