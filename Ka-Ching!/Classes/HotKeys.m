//
//  HotKeys.m
//  Ka-Ching!
//
//  Created by Petros on 05-01-18.
//  Copyright (c) 2017-2018 Kinoko House. MIT License.
//


# pragma mark Includes

#import "HotKeys.h"
#import "AppDelegate.h"


#pragma mark Constants

// number of hotkeys allowed
const BOOL NUMBER_OF_HOTKEYS = 8;


@interface HotkeyCenter()

#pragma mark Unexposed Class Properties


@property BOOL isPaused;

@end

@implementation HotkeyCenter



#pragma mark Class Constructors & Destructors

- (id)init {
    if (self == [super init]) {
        _hotkeys = [NSMutableArray arrayWithCapacity:NUMBER_OF_HOTKEYS];
        for (int i = 0; i < NUMBER_OF_HOTKEYS; i++) {
            [_hotkeys addObject:[NSNull null]];
        }
        EventTypeSpec eventType;
        eventType.eventClass = kEventClassKeyboard;
        eventType.eventKind = kEventHotKeyPressed;
        InstallApplicationEventHandler(&HotKeyEventHandler, 1, &eventType, NULL, NULL);
        _isPaused = NO;
    }
    return self;
}

- (void) dealloc {
    [self unregisterHotkeys];
    OSStatus RemoveEventHandler(EventHandlerRef HotKeyEventHandler);
}


#pragma mark Class Methods: Hotkey Registration

- (void)setHotkey:(unsigned long)keyCode withModifiers:(unsigned long)modMask andRepresentation:(NSString *)keyString forTaskID:(int)taskid activate:(BOOL)flag {
    if (((taskid < 0) || (taskid > (NUMBER_OF_HOTKEYS - 1))) || [keyString isEqualToString:@""] || keyString == nil) {
        return;
    }
    if (![[_hotkeys objectAtIndex:taskid] isNotEqualTo:[NSNull null]]) {
        [self unregisterHotkeyForTaskId:taskid];
    }
    UInt32 modflags = 0;
    if ((modMask & NSShiftKeyMask) > 0) {
        modflags += shiftKey;
    }
    if ((modMask & NSControlKeyMask) > 0) {
        modflags += controlKey;
    }
    if ((modMask & NSAlternateKeyMask) > 0) {
        modflags += optionKey;
    }
    if ((modMask & NSCommandKeyMask) > 0) {
        modflags += cmdKey;
    }
    if (flag) {
        EventHotKeyRef gMyHotKeyRef;
        EventHotKeyID gMyHotKeyID;
        OSType myType = NSHFSTypeCodeFromFileType([NSString stringWithFormat:@"kCh%i", taskid]);
        gMyHotKeyID.signature = myType;
        gMyHotKeyID.id = (UInt32)taskid;
        RegisterEventHotKey((UInt32)keyCode, modflags, gMyHotKeyID, GetApplicationEventTarget(), 0, &gMyHotKeyRef);
        NSValue *refStruct = [NSValue valueWithBytes:&gMyHotKeyRef objCType:@encode(EventHotKeyRef)];
        NSValue *htkStruct = [NSValue valueWithBytes:&gMyHotKeyRef objCType:@encode(EventHotKeyID)];
        NSMutableArray *hotKeyMorsel = [NSMutableArray arrayWithCapacity:5];
        [hotKeyMorsel addObject:[NSNumber numberWithUnsignedLong:keyCode]];
        [hotKeyMorsel addObject:[NSNumber numberWithUnsignedLong:modMask]];
        [hotKeyMorsel addObject:keyString];
        [hotKeyMorsel addObject:refStruct];
        [hotKeyMorsel addObject:htkStruct];
        [_hotkeys replaceObjectAtIndex:(int)taskid withObject:hotKeyMorsel];
    } else {
        NSMutableArray *hotKeyMorsel = [NSMutableArray arrayWithCapacity:5];
        [hotKeyMorsel addObject:[NSNumber numberWithUnsignedLong:keyCode]];
        [hotKeyMorsel addObject:[NSNumber numberWithUnsignedLong:modMask]];
        [hotKeyMorsel addObject:keyString];
        [hotKeyMorsel addObject:@""];
        [hotKeyMorsel addObject:@""];
        [_hotkeys replaceObjectAtIndex:(int)taskid withObject:hotKeyMorsel];
    }
}

- (void)unsetHotkeyForTaskId:(int)taskid pauseOnly:(BOOL)pauseonly deactivate:(BOOL)deactivate {
    if (taskid < 0 || taskid > (NUMBER_OF_HOTKEYS - 1)) {
        return;
    }
    if (pauseonly && deactivate) {
        return;
    }
    NSMutableArray *hotKeyMorsel = [_hotkeys objectAtIndex:taskid];
    if ([hotKeyMorsel isEqual:[NSNull null]]) {
        return;
    }
    if (!_isPaused) {
        if (pauseonly || deactivate) {
            EventHotKeyRef gMyHotKeyRef;
            NSValue *myValue = [hotKeyMorsel objectAtIndex:3];
            [myValue getValue:&gMyHotKeyRef];
            UnregisterEventHotKey(gMyHotKeyRef);
        }
    }
    if (!pauseonly) {
        [_hotkeys replaceObjectAtIndex:taskid withObject:[NSNull null]];
    }
}

- (void)registerHotkey:(unsigned long)keyCode withModifiers:(unsigned long)modMask andRepresentation:(NSString *)keyString forTaskID:(int)taskid {
    if (!_isPaused) {
        [self setHotkey:keyCode withModifiers:modMask andRepresentation:keyString forTaskID:taskid activate:YES];
    } else {
        [self setHotkey:keyCode withModifiers:modMask andRepresentation:keyString forTaskID:taskid activate:NO];
    }
}

- (void)unregisterHotkeyForTaskId:(int)taskid {
    if (!_isPaused) {
        [self unsetHotkeyForTaskId:taskid pauseOnly:NO deactivate:YES];
    } else {
        [self unsetHotkeyForTaskId:taskid pauseOnly:NO deactivate:NO];
    }
}

- (void)pauseHotkeyForTaskId:(int)taskid {
    [self unsetHotkeyForTaskId:taskid pauseOnly:YES deactivate:NO];
}

- (void)unregisterHotkeys {
    for (int i = 0; i < NUMBER_OF_HOTKEYS; i++) {
        [self unregisterHotkeyForTaskId:i];
    }
}

- (void)pauseHotkeys {
    if (_isPaused) {
        return;
    }
    for (int i = 0; i < NUMBER_OF_HOTKEYS; i++) {
        [self pauseHotkeyForTaskId:i];
    }
    _isPaused = YES;
}

- (void)unpauseHotkeys {
    if (!_isPaused) {
        return;
    }
    _isPaused = NO;
    NSArray *hotkeysToResume = [_hotkeys copy];
    [self registerHotkeysFromArray:hotkeysToResume];
}


#pragma mark Class Methods: Utilities

- (NSArray *)getHotkeysAsArray {
    return _hotkeys;
}

- (NSArray *)getHotkeyForTaskIdAsArray:(int)taskid {
    if (taskid < 0 || taskid > (NUMBER_OF_HOTKEYS - 1)) {
        return nil;
    }
    return _hotkeys[taskid];
}

- (void)registerHotkeysFromArray:(NSArray *)array {
    NSArray *hotKeyMorsel;
    NSArray *preliminaryCopy;
    _hotkeys = [array mutableCopy];
    for (int i = 0; i < NUMBER_OF_HOTKEYS; i++) {
        preliminaryCopy = [_hotkeys objectAtIndex:i];
        if (![preliminaryCopy isEqual:[NSNull null]]) {
            hotKeyMorsel = [preliminaryCopy mutableCopy];
            UInt32 keycode = (UInt32)[[hotKeyMorsel objectAtIndex:0] unsignedLongValue];
            UInt32 modmask = (UInt32)[[hotKeyMorsel objectAtIndex:1] unsignedLongValue];
            NSString *keystring = [hotKeyMorsel objectAtIndex:2];
            [self registerHotkey:keycode withModifiers:modmask andRepresentation:keystring forTaskID:i];
        } else {
            [self unregisterHotkeyForTaskId:i];
        }
    }
}

- (NSString *)hotKeyToMenuKeyEquivalent:(int)keycode {
    if (!_keyEquivDict) {
        _keyEquivDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                      @0x00, @"a",
                                      @0x0B, @"b",
                                      @0x08, @"c",
                                      @0x02, @"d",
                                      @0x0E, @"e",
                                      @0x03, @"f",
                                      @0x05, @"g",
                                      @0x04, @"h",
                                      @0x22, @"i",
                                      @0x26, @"j",
                                      @0x28, @"k",
                                      @0x25, @"l",
                                      @0x2E, @"m",
                                      @0x2D, @"n",
                                      @0x1F, @"o",
                                      @0x23, @"p",
                                      @0x0C, @"q",
                                      @0x0F, @"r",
                                      @0x01, @"s",
                                      @0x11, @"t",
                                      @0x20, @"u",
                                      @0x09, @"v",
                                      @0x0D, @"w",
                                      @0x07, @"x",
                                      @0x10, @"y",
                                      @0x06, @"z",
                                      @0x1D, @"0",
                                      @0x12, @"1",
                                      @0x13, @"2",
                                      @0x14, @"3",
                                      @0x15, @"4",
                                      @0x17, @"5",
                                      @0x16, @"6",
                                      @0x1A, @"7",
                                      @0x1C, @"8",
                                      @0x19, @"9",
                                      @0x32, @"`",
                                      @0x0A, @"§",
                                      @0x2B, @",",
                                      @0x2F, @".",
                                      @0x1B, @"-",
                                      @0x18, @"=",
                                      @0x2C, @"/",
                                      @0x2A, @"\\",
                                      @0x27, @"'",
                                      @0x29, @";",
                                      @0x1E, @"]",
                                      @0x21, @"[",
                                      @0x35, @"⎋",
                                      @0x24, @"↩",
                                      @0x4C, @"⌤",
                                      @0x30, @"⇥",
                                      @0x33, @"⌫",
                                      @0x7B, @"←",
                                      @0x7C, @"→",
                                      @0x7D, @"↓",
                                      @0x7E, @"↑",
                                      nil];
    }
    NSString *keyEquiv;
    unichar c;
    NSArray *keyArray;
    switch (keycode) {
        case 0x7A:
            c = NSF1FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x78:
            c = NSF2FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x63:
            c = NSF3FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x76:
            c = NSF4FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x60:
            c = NSF5FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x61:
            c = NSF6FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x62:
            c = NSF7FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x64:
            c = NSF8FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x65:
            c = NSF9FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x6D:
            c = NSF10FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x67:
            c = NSF11FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x6F:
            c = NSF12FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x69:
            c = NSF13FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x6B:
            c = NSF14FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x71:
            c = NSF15FunctionKey;
            keyEquiv = [NSString stringWithCharacters:&c length:1];
            break;
        case 0x31:
            keyEquiv = @" ";
            break;
        default:
            keyArray = [_keyEquivDict allKeysForObject:[NSNumber numberWithInt:keycode]];
            if (keyArray == nil) {
                NSLog(@"Error converting hotkey to menu key equivalent");
                keyEquiv = @"§";
                break;
            }
            keyEquiv = [keyArray objectAtIndex:0];
        }
    return keyEquiv;
}

- (NSArray *)prepHotkeysForPropertyListStorage {
    NSMutableArray *newhotkeys = [_hotkeys mutableCopy];
    NSMutableArray *hotKeyMorsel;
    for (int i = 0; i < 8; i++) {
        if ([newhotkeys[i] isNotEqualTo:[NSNull null]]) {
            hotKeyMorsel = [newhotkeys[i] mutableCopy];
            [hotKeyMorsel replaceObjectAtIndex:3 withObject:@""];
            [hotKeyMorsel replaceObjectAtIndex:4 withObject:@""];
            [newhotkeys replaceObjectAtIndex:i withObject:hotKeyMorsel];
        } else {
            hotKeyMorsel = [NSMutableArray arrayWithCapacity:5];
            hotKeyMorsel = [@[@0, @0, @"", @"", @""] mutableCopy];
            [newhotkeys replaceObjectAtIndex:i withObject:hotKeyMorsel];
        }
    }
    return newhotkeys;
}
- (BOOL)validateInput:(id)obj forKeyCombo:(NSString *)keycombo checkRegisteredHotkeysOnly:(BOOL)flag {
    AppDelegate *appDelegate = (AppDelegate *)[NSApp delegate];
    NSArray *theNecessities = [appDelegate getHotkeyNecessities];
    NSArray <HotkeyTextView *> *hotKeyTextViewCollection  = theNecessities[0];
    NSUInteger buttonID = [theNecessities[1] integerValue];
    int pointer = [obj intValue];
    if (pointer == 99) {
        pointer = (int)buttonID;
    }
    NSString *currentString;
    if (!flag) {
        if (hotKeyTextViewCollection != nil) {
            for (int i = 0; i < [hotKeyTextViewCollection count]; i++) {
                if (i != pointer) {
                    currentString = [hotKeyTextViewCollection objectAtIndex:i].keyString;
                    if ([currentString isEqualToString:keycombo]) {
                        return NO;
                    }
                }
            }
        }
    }
    NSArray *hotKeys = [self getHotkeysAsArray];
    for (int i = 0; i < [hotKeys count]; i++) {
        if (i != pointer) {
            NSArray *hotKeyMorsel = [hotKeys objectAtIndex:i];
            if (![hotKeyMorsel isEqual:[NSNull null]]) {
                currentString = [hotKeyMorsel objectAtIndex:2];
                if ([currentString isEqualToString:keycombo]) {
                    return NO;
                }
            }
        }
    }
    return YES;
}

#pragma mark Carbon Keyboard Event Handler

OSStatus HotKeyEventHandler(EventHandlerCallRef nextHandler,EventRef theEvent, void *userData) {
    AppDelegate *appDelegate = (AppDelegate *) [NSApp delegate];
    EventHotKeyID hkRef;
    GetEventParameter(theEvent,kEventParamDirectObject,typeEventHotKeyID,NULL,sizeof(hkRef),NULL,&hkRef);
    [appDelegate respondToHotkey:(int)hkRef.id];
    return noErr;
}

@end
