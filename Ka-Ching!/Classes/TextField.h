//
//  TextField.h
//  Ka-Ching!
//
//  Created by Petros on 05-01-18.
//  Copyright (c) 2017-2018 Kinoko House. MIT License.
//

#import <Cocoa/Cocoa.h>
#import <AppKit/AppKit.h>
#import <Foundation/Foundation.h>

@interface TextField : NSTextField

- (void)textDidChange:(NSNotification *)notification;

@end
