# Ka-Ching! #

Stupid soundboard in honour of the guys of the video department. Today we celebrate that, as we dared hope back then, the day has come that this useless bit of software has indeed culminated into a fully editable and customisable soundboard with hotkeys and floating panels :). You probably still won't care for it anyway. Ready to use binaries (10.10+) in the downloads section.

Current version is 2.1 (build 6). New in this version:

- Almost completely rebuilt from scratch, with too many new features to mention;

- There are now three soundboards, of which two are customisable (MP3, WAV or AIFF) by way of drag and drop;

- Hotkeys are now also customisable (using a primitive hotkey textview kind of affair);

- English and Dutch localisation;

- Help file is included.

There's probably bugs in there, tread with care.
